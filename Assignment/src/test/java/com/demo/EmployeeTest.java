package com.demo; 
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import com.demo.controllers.MainController;
import com.demo.dao.EmployeeDAO;
import com.demo.models.Employee;
import com.demo.services.EmployeeService;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.Set;

import javax.validation.ConstraintViolation;

@RunWith(SpringRunner.class)
//@DataJpaTest
public class EmployeeTest {
       private MockMvc mockMvc;
       private LocalValidatorFactoryBean localValidatorFactory;


       @Autowired
       private TestEntityManager entityManager;
    
   /*    @Autowired
       private EmployeeService employeeService;*/
       
       @Before
        public void setUp() {
         mockMvc = MockMvcBuilders.standaloneSetup(new MainController()).build();
        }
       /* @Test
        public void testIndex() throws Exception{
               this.mockMvc.perform(get("/employee/findAll"))
                    .andExpect(status().isOk())
                    .andExpect(view().name("employee"))
                    .andDo(print());
        }
        */
       @Test 
            public void testNameWithInvalidCharCausesValidationError() {
            final Employee employee = new Employee();
            employee.setFirstName("A FirstName with\t a Tab character");
            employee.setLastName("A LastName with\t a Tab character");
	         Set<ConstraintViolation<Employee>> constraintViolations = localValidatorFactory.validate(employee);
	         Assert.assertTrue("Expected validation error not found", constraintViolations.equals("^[a-zA-Z0-9]*$"));//constraintViolations.size() == 11
          }
         
        /*@Test
        public void whenFindByFirstName_thenReturnEmployee() {
            // given
            Employee randy = new Employee("Randy","Orton");
            entityManager.persist(randy);
            entityManager.flush(); 
            // when
            Employee found = employeeService.findById(randy.getId()); 
            // then
            assertThat(found.getFirstName()).isEqualTo(randy.getFirstName());
        }
        @Test
        public void whenFindByFLastName_thenReturnEmployee() {
            // given
            Employee randy = new Employee("Randy","Orton");
            entityManager.persist(randy);
            entityManager.flush(); 
            // when
            Employee found = employeeService.findById(randy.getId()); 
            // then
            assertThat(found.getLastName()).isEqualTo(randy.getLastName());
        }*/

  }