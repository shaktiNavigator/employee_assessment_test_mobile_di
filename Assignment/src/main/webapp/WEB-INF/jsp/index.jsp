<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h1>Welcome To Assignment Application</h1>
  <p></p>                                                                                      
  <div class="table-responsive">          
  <table class="table">
    <thead>
      <tr>
        <th>Route</th>
        <th>Use-case</th> 
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><a href="http://localhost:9000/employee/findAll">/employee/findAll</a></td>
        <td>To find all the Employees stored in DB</td> 
      </tr>
    </tbody>
  </table>
  </div>
  
  <br>
  
    <h2>Configuration Values</h2>
  <p></p>                                                                                      
  <div class="table-responsive">          
  <table class="table">
    <thead>
      <tr>
        <th>port</th>
        <th>value</th> 
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>server.port</td>
        <td>9000</td> 
      </tr>
      <tr>
        <td>spring.datasource.url</td>
        <td>jdbc:mysql://localhost:3306/organization</td> 
      </tr>
      <tr>
        <td>spring.datasource.username</td>
        <td>root</td> 
      </tr>
      <tr>
        <td>spring.datasource.password</td>
        <td>root</td> 
      </tr>
      <tr>
        <td>Create Schema Query</td>
        <td>create database organization;</td> 
      </tr>
      <tr>
        <td>Select the Schema</td>
        <td>use organization;</td> 
      </tr>
      <tr>
        <td>Create Table Query</td>
        <td>CREATE TABLE Employee (
		    id INT NOT NULL AUTO_INCREMENT,
		    last_name varchar(10),
		    first_name varchar(10),
		    PRIMARY KEY ( id )
		     );
		</td> 
      </tr>
      <tr>
        <td>Insert a row</td>
        <td>INSERT INTO Employee(first_name , last_name)
		    VALUES ("Steve", "Austin");
		</td> 
      </tr>
      <tr>
        <td>logging.file</td>
        <td>logs/spring-boot-logging.log</td> 
      </tr>
    </tbody>
  </table>
  </div>
</div>

</body>
</html>
