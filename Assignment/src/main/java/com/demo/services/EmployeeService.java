package com.demo.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.dao.EmployeeDAO;
import com.demo.models.Employee;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeDAO employeeDAO;
	
	public Collection<Employee> findAll(){
		/*List<Employee> employees= new ArrayList<Employee>();
		for (Employee employee : employeeDAO.findAll()) {
			employees.add(employee); 
		}
		return employees;*/
	return (Collection<Employee>) employeeDAO.findAll();
	}
    public Employee findById(Long id){
    	return employeeDAO.findById(id).orElse(new Employee());
    }; 
    
     
}
