package com.demo.controllers;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.demo.models.Employee;
import com.demo.services.EmployeeService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
 
@Controller
@RequestMapping("employee")
public class Employees {


	private static final Logger logger = LoggerFactory.getLogger(Employees.class);
	@Autowired
	private EmployeeService employeeService;
	 
	@GetMapping("/findAll")
	public StringBuffer  getAllEmployees(){
		Collection<Employee> employees= employeeService.findAll();
		ObjectMapper mapper = new ObjectMapper();
		StringBuffer output= new StringBuffer();
		for (Employee employee : employees) {   
			try {
				String employeeString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(employee);
				output.append(employeeString);
				output.append("\n");
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
			}  
		logger.info(output.toString());
		/*return new ModelAndView("employee", "employees", output); */
		return output;
	}

}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
