package com.demo.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity(name="employee")
public class Employee {

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private long id;
	
	@NotNull
    @Size(max=10)
    @Pattern(regexp="^[a-zA-Z0-9]*$", message="FirstName must not contain any special symbol, only Alfanumeric is allowed")
	@Column(name="first_name")
	private String FirstName;

	@NotNull
    @Size(max=10)
    @Pattern(regexp="^[a-zA-Z0-9]*$", message="LastName must not contain any special symbol, only Alfanumeric is allowed")
	@Column(name="last_name") // this should be same as there in database
	private String LastName;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public Employee(String firstName, String lastName) { 
		FirstName = firstName;
		LastName = lastName;
	}

	public Employee( ) {  
	}
}
