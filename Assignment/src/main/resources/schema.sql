DROP TABLE IF EXISTS Employee ;
CREATE TABLE Employee (
    id INT NOT NULL AUTO_INCREMENT,
    last_name varchar(10),
    first_name varchar(10),
   PRIMARY KEY ( id )
);
